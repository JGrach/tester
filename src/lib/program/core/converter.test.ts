import { expect } from 'chai'
import {  parseWithNodeMaps } from "@typescript-eslint/typescript-estree"
import { esStringToAst, jsAstToString, tsAstToString } from './converters'

describe('core/converters', () => {
    const tsString = `function a(b: number, c:number, d:number){ return b + c + d }`
    const jsString = `function a(b, c, d){ return b + c + d }`
    const { ast: tsNode , esTreeNodeToTSNodeMap } = parseWithNodeMaps(tsString)

    describe('esStringToAst', () => {
        it('should return an AST', () => {
            const { program } = esStringToAst(tsString, { lang: 'ts' })

            expect(program).deep.equal(tsNode)
        })
        it('should return an error on bad code', () => {
            try {
                esStringToAst(tsString + `bad string`, { lang: 'ts' })
            } catch (e) {
                expect(e).instanceOf(Error)
                expect(e.toString()).equal("TSError: ';' expected.")
            }
        })
    })

    describe('jsAstToString', () => {
        it('should return a js string', () => {
            const result = jsAstToString(tsNode)

            expect(result.replace(/[\s;]/g, '')).deep.equal(jsString.replace(/[\s;]/g, ''))
        })
    })

    describe('tsAstToString', () => {
        it('should return a ts string', () => {
            const result = tsAstToString(tsNode, esTreeNodeToTSNodeMap)

            expect(result.replace(/[\s;]/g, '')).deep.equal(tsString.replace(/[\s;]/g, ''))
        })
    })
})