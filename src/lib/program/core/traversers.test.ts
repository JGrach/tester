import { expect } from 'chai'
import { spy } from 'sinon'
import {  parse } from "@typescript-eslint/typescript-estree"
import type { TSESTree } from "@typescript-eslint/types";
import { findNodeInto, filterNodesInto, reduceNodesInto } from './traversers'

describe('core/traversers', () => {
    const tsString = `function a(b: number, c:number, d:number){ return b + c + d }`
    const tsNode = parse(tsString)

    describe('findNodeInto', () => {
        it('should return the first identifier', () => {
            const cb = (node: TSESTree.Node) => node.type === 'Identifier'

            const result = findNodeInto(tsNode, cb)

            expect(result).deep.equal({ type: 'Identifier', name: 'a' })
        })
        it('should return undefined', () => {
            const cb = spy((node) => node.type === 'ClassDeclaration')

            const result = findNodeInto(tsNode, cb)

            expect(result).equal(undefined)
        })
    })

    describe('filterNodesInto', () => {
        it('should return all binary Expressions', () => {
            const cb = (node: TSESTree.Node) => node.type === 'BinaryExpression'

            const result = filterNodesInto(tsNode, cb)

            expect(result).deep.equal([{
                type: 'BinaryExpression',
                operator: '+',
                left: {
                  type: 'BinaryExpression',
                  operator: '+',
                  left: { type: 'Identifier', name: 'b' },
                  right: { type: 'Identifier', name: 'c' }
                },
                right: { type: 'Identifier', name: 'd' }
              }, {
                type: 'BinaryExpression',
                operator: '+',
                left: { type: 'Identifier', name: 'b' },
                right: { type: 'Identifier', name: 'c' }
              }])
        })
        it('should return empty array', () => {
            const cb = (node: TSESTree.Node) => node.type === 'ClassDeclaration'

            const result = filterNodesInto(tsNode, cb)

            expect(result).deep.equal([])
        })
    })

    describe('reduceNodesInto', () => {
        it('should return an accumulator for traverse entrance', () => {
            const cb = (acc: number, node: TSESTree.Node) => node.type === 'Identifier' ? acc + 1 : acc

            const result = reduceNodesInto(0, tsNode, cb)

            expect(result).equal(7)
        })
        it('should return an accumulator for traverse exit', () => {
            const cb = (acc: number, node: TSESTree.Node) => node.type === 'Identifier' ? acc + 1 : acc

            const result = reduceNodesInto(0, tsNode, undefined, cb)

            expect(result).equal(7)
        })
        it('should return an accumulator for traverse all', () => {
            const cbEnter = (acc: number, node: TSESTree.Node) => node.type === 'Identifier' ? acc + 1 : acc
            const cbExit = (acc: number, node: TSESTree.Node) => node.type === 'Identifier' ? acc - 1 : acc

            const result = reduceNodesInto(0, tsNode, cbEnter, cbExit)

            expect(result).equal(0)
        })
    })
})