import type { Controller, Visitor } from 'estraverse'
import type { TSESTree } from "@typescript-eslint/types";

import { visitorKeys } from "@typescript-eslint/visitor-keys"
import { traverse } from 'estraverse'

function toVisit(node: TSESTree.Node, enter: Visitor['enter'], leave?: Visitor['leave']) {
    const visitor: Visitor = {
        keys: visitorKeys, 
        enter,
        leave,
    }

    traverse(node, visitor)
}

type CheckerFn = (node: TSESTree.Node, parent: TSESTree.Node | null, control: Controller) => boolean
type ReducerFn<T> = (accumulator: T, node: TSESTree.Node, parent: TSESTree.Node | null, control: Controller) => T

/**
 * Find a node among all descendant nodes that validates predicate function 
 * @param nodeAst ast node to look in
 * @param checker predicate function to apply on all AST descendant nodes
 * @returns the first node validating the predicate or undefined
 */
export function findNodeInto<T extends TSESTree.Node>(nodeAst: T, checker: CheckerFn): TSESTree.Node | undefined {
    let nodeResult: TSESTree.Node | undefined = undefined

    const findNodeAction: Visitor['enter'] | Visitor['leave'] = function(node, parent) {
        if (checker(node, parent, this)) {
            nodeResult = node 
            this.break();
        }
    }

    toVisit(nodeAst, findNodeAction)

    return nodeResult
}

/**
 * Find all nodes among all descendant nodes that validates predicate function 
 * @param nodeAst ast node to look in
 * @param checker predicate function to apply on all AST descendant nodes
 * @returns an array of all nodes validating the predicate
 */
export function filterNodesInto<T extends TSESTree.Node>(nodeAst: T, checker: CheckerFn) {
    const nodes: TSESTree.Node[] = []

    const filterNodeAction: Visitor['enter'] | Visitor['leave'] = function(node, parent) {
        if (checker(node, parent, this)) nodes.push(node)
    }

    toVisit(nodeAst, filterNodeAction)

    return nodes
}

/**
 * On each descendant node, apply a function on the enter then apply a function on the leave.
 * Return is an accumulator updated by the two functions.
 * @param accumulator a new value to be updating for each node
 * @param nodeAst ast node to look in
 * @param onEnter callback function applying on enter node
 * @param onLeave optional callback function applying on leave node
 * @returns accumulator updated
 */
export function reduceNodesInto<T extends TSESTree.Node, U>(accumulator: U, nodeAst: T, onEnter: ReducerFn<U> | void, onLeave?: ReducerFn<U>) {
    const updateAccOnEnter: Visitor['enter'] = onEnter ? 
        function(node, parent) {
            accumulator = onEnter(accumulator, node, parent, this)
        } : undefined

    const updateAccOnLeave: Visitor['leave'] = onLeave ? 
        function(node, parent) {
            accumulator = onLeave(accumulator, node, parent, this)
        } : undefined

    toVisit(nodeAst, updateAccOnEnter, updateAccOnLeave)

    return accumulator
}
