import type { ParserWeakMapESTreeToTSNode, TSESTreeOptions } from "@typescript-eslint/typescript-estree/dist/parser-options";
import type { AST } from "@typescript-eslint/typescript-estree"

import { parseWithNodeMaps } from "@typescript-eslint/typescript-estree"
import { generate } from 'escodegen'
import ts from 'typescript';

export type ProgramOptions = {
    lang: 'ts' | 'js'
} & TSESTreeOptions

type AstRepresentation = {
    program: AST<ProgramOptions>,
    esTreeNodeToTSNodeMap: ParserWeakMapESTreeToTSNode, 
}

/**
 * Change an EcmaScript string code into an AST representation
 * @param esString an EcmaScript string code
 * @param options an object option
 * @returns an AST representation
 */
export function esStringToAst(esString: string, options: ProgramOptions): AstRepresentation {
    const { ast: program , esTreeNodeToTSNodeMap } = parseWithNodeMaps(esString, options)
    return {
        program, 
        esTreeNodeToTSNodeMap, 
    }
}

/**
 * Change an AST representation into a Javascript string
 * @param program an AST program representation
 * @returns a JavaScript string
 */
 export function jsAstToString(program: AstRepresentation['program']) {
    return generate(program)
}

/**
 * Change an AST representation into a TypeScript string
 * @param program AST program representation
 * @param esTreeNodeToTSNodeMap a converter map between esTreeNode and tsNode
 * @returns a TypeScript string
 */
export function tsAstToString(program: AstRepresentation['program'], esTreeNodeToTSNodeMap: AstRepresentation['esTreeNodeToTSNodeMap']) {
    const printableAst = esTreeNodeToTSNodeMap.get(program)
    const printer = ts.createPrinter({ newLine: ts.NewLineKind.LineFeed });
    
    return printer.printNode(ts.EmitHint.Unspecified, printableAst, printableAst);
}