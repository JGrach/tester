import type { TSESTree } from "@typescript-eslint/types";

import { AST_NODE_TYPES } from "@typescript-eslint/types";
import { getIdentifiersName } from '../identifiers'

type ExportedNodes = {
    [k: string]: TSESTree.ExportDeclaration | TSESTree.Expression | null
}

function getExportIdentifiers(node: TSESTree.ExportNamedDeclaration) {
    const nodeIdentifiers: (Exclude<TSESTree.ExportDeclaration, TSESTree.ClassExpression> | TSESTree.Identifier)[] = [
        ...node.specifiers.map(({ exported }) => exported)
    ]

    if (node.declaration && node.declaration.type !== AST_NODE_TYPES.ClassExpression) {
        nodeIdentifiers.push(node.declaration)
    }

    return nodeIdentifiers.flatMap(nodeIdentifier => getIdentifiersName(nodeIdentifier))
}

function onExportNamed(node: TSESTree.ExportNamedDeclaration) {
    const identifiers = getExportIdentifiers(node)
    return identifiers.reduce<ExportedNodes>((acc, identifier) => {
        return { ...acc, [identifier]: node.declaration }
    }, {})
}

function onExportDefault(node: TSESTree.ExportDefaultDeclaration) {
    return { 'default': node.declaration }
}

export function getExportedNode(node: TSESTree.Node): ExportedNodes {
    if (node.type === AST_NODE_TYPES.ExportNamedDeclaration) return onExportNamed(node)
    if (node.type === AST_NODE_TYPES.ExportDefaultDeclaration) return onExportDefault(node)
    return {}
}