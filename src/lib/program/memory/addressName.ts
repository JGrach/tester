import { Controller } from 'estraverse';
import { reduceNodesInto, TSESTree } from '../core';

import { AST_NODE_TYPES } from '../core';
import { AssignmentNode } from './memoryManipulatingNode';

function getNameFromAnyNode(node: TSESTree.Node, control?: Controller): string | undefined {
    // TODO: to be exhaustived
    // TODO: !readbility! split this function
    if (node.type === AST_NODE_TYPES.ImportSpecifier) {
        if (control) control.skip()
        return getNameFromAnyNode(node.local)
    }
    if (node.type === AST_NODE_TYPES.Property) {
        if (control) control.skip()
        return getNameFromAnyNode(node.key)
    }
    if (node.type === AST_NODE_TYPES.MemberExpression) {
        if (control) control.skip()
        return getNameFromAnyNode(node.object)
    }
    if (node.type === AST_NODE_TYPES.Identifier) return node.name
    if (node.type === AST_NODE_TYPES.Literal) return typeof node.value === 'string' ? node.value: undefined
    return undefined
}

function aggregNames(names: string[], node: TSESTree.Node, _: TSESTree.Node | null, control: Controller) {
    const name = getNameFromAnyNode(node, control)

    return name ? [...names, name] : names
}

function getLeftSideNodes(node: AssignmentNode | TSESTree.VariableDeclarator): TSESTree.Node[] {
    // TODO: !readbility! variableDeclarator shouldn't be here 
    if (node.type === AST_NODE_TYPES.AssignmentExpression) return [node.left]
    if (node.type === AST_NODE_TYPES.ImportDeclaration) return node.specifiers
    if (node.type === AST_NODE_TYPES.VariableDeclaration) return node.declarations.flatMap(getLeftSideNodes)
    if (node.type === AST_NODE_TYPES.VariableDeclarator) return [node.id]
    
    return !node.id ? [] : [node.id]
}

/**
 * get all the string identifier for a assignmentNode
 * @param assignmentNode an assignment node
 * @returns an array of all identifiers name manipulated by the assignment node
 */
export function getIdentifiersName(assignmentNode: AssignmentNode) {
    const leftSideNodes: TSESTree.Node[] = getLeftSideNodes(assignmentNode)

    // for each node, apply aggregNamesFromNamedNode on all deep node
    return leftSideNodes.reduce<string[]>((names, node) => reduceNodesInto(names, node, aggregNames), [])
}