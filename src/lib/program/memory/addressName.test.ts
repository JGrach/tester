import { expect } from 'chai'
import {  parse } from "@typescript-eslint/typescript-estree"
import { checkNodeIsAssignmentNode, AssignmentNode } from './memoryManipulatingNode'

import { getIdentifiersName } from './addressName'
import { findNodeInto } from '../core'

describe('memory/addressName', () => {
    describe('getIdentifiersName', () => {
        it('should return left name for assignement pattern', () => {
            const tsString = `a = b`
            const tsNode = parse(tsString)
            const assignmentNode = findNodeInto(tsNode, checkNodeIsAssignmentNode) as AssignmentNode

            const result = getIdentifiersName(assignmentNode)

            expect(result).deep.equal(['a'])
        })
        it('should return left name for class declaration', () => {
            const tsString = `class MyClass {}`
            const tsNode = parse(tsString)
            const assignmentNode = findNodeInto(tsNode, checkNodeIsAssignmentNode) as AssignmentNode

            const result = getIdentifiersName(assignmentNode)

            expect(result).deep.equal(['MyClass'])
        })
        it('should return left name for function declaration', () => {
            const tsString = `function myFunction() {}`
            const tsNode = parse(tsString)
            const assignmentNode = findNodeInto(tsNode, checkNodeIsAssignmentNode) as AssignmentNode

            const result = getIdentifiersName(assignmentNode)

            expect(result).deep.equal(['myFunction'])
        })
        it('should return left name for default ImportDeclaration', () => {
            const tsString = `import a from 'b'`
            const tsNode = parse(tsString)
            const assignmentNode = findNodeInto(tsNode, checkNodeIsAssignmentNode) as AssignmentNode

            const result = getIdentifiersName(assignmentNode)

            expect(result).deep.equal(['a'])
        })
        it('should return left name for named ImportDeclaration', () => {
            const tsString = `import {a, b, c} from 'd'`
            const tsNode = parse(tsString)
            const assignmentNode = findNodeInto(tsNode, checkNodeIsAssignmentNode) as AssignmentNode

            const result = getIdentifiersName(assignmentNode)

            expect(result).deep.equal(['a', 'b', 'c'])
        })
        it('should return left name for named and aliased ImportDeclaration', () => {
            const tsString = `import {a as b} from 'c'`
            const tsNode = parse(tsString)
            const assignmentNode = findNodeInto(tsNode, checkNodeIsAssignmentNode) as AssignmentNode

            const result = getIdentifiersName(assignmentNode)

            expect(result).deep.equal(['b'])
        })
        it('should return left name for default and aliased ImportDeclaration', () => {
            const tsString = `import * as a from 'b'`
            const tsNode = parse(tsString)
            const assignmentNode = findNodeInto(tsNode, checkNodeIsAssignmentNode) as AssignmentNode

            const result = getIdentifiersName(assignmentNode)

            expect(result).deep.equal(['a'])
        })
        it('should return left name for MemberExpression assignment', () => {
            const tsString = `a.b.c = d.e`
            const tsNode = parse(tsString)
            const assignmentNode = findNodeInto(tsNode, checkNodeIsAssignmentNode) as AssignmentNode

            const result = getIdentifiersName(assignmentNode)

            expect(result).deep.equal(['a'])
        })
        it('should return left name for VariableDeclaration', () => {
            const tsString = `const a = b`
            const tsNode = parse(tsString)
            const assignmentNode = findNodeInto(tsNode, checkNodeIsAssignmentNode) as AssignmentNode

            const result = getIdentifiersName(assignmentNode)

            expect(result).deep.equal(['a'])
        })
        it('should return left name for VariableDeclaration with ObjectPatern', () => {
            const tsString = `const { a, b, c } = d`
            const tsNode = parse(tsString)
            const assignmentNode = findNodeInto(tsNode, checkNodeIsAssignmentNode) as AssignmentNode

            const result = getIdentifiersName(assignmentNode)

            expect(result).deep.equal(['a', 'b', 'c'])
        })
        it('should return left name for VariableDeclaration with ObjectPatern and rest', () => {
            const tsString = `const { a, b, ...c } = d`
            const tsNode = parse(tsString)
            const assignmentNode = findNodeInto(tsNode, checkNodeIsAssignmentNode) as AssignmentNode

            const result = getIdentifiersName(assignmentNode)

            expect(result).deep.equal(['a', 'b', 'c'])
        })
        it('should return left name for VariableDeclaration with ArrayPatern', () => {
            const tsString = `const [a, b, c] = d`
            const tsNode = parse(tsString)
            const assignmentNode = findNodeInto(tsNode, checkNodeIsAssignmentNode) as AssignmentNode

            const result = getIdentifiersName(assignmentNode)

            expect(result).deep.equal(['a', 'b', 'c'])
        })
    })
})