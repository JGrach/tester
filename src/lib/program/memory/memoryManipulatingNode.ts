import type { TSESTree } from '../core';

import { AST_NODE_TYPES } from '../core';

export type AssignmentNode = TSESTree.AssignmentExpression |
    TSESTree.ClassDeclaration |
    TSESTree.FunctionDeclaration |
    TSESTree.ImportDeclaration |
    TSESTree.TSDeclareFunction |
    TSESTree.TSEnumDeclaration |
    TSESTree.TSInterfaceDeclaration |
    TSESTree.TSModuleDeclaration |
    TSESTree.TSTypeAliasDeclaration |
    TSESTree.VariableDeclaration


export function checkNodeIsAssignmentNode(node: TSESTree.Node): node is AssignmentNode {
    // TODO: to be exhaustived for sure
    return node.type === AST_NODE_TYPES.AssignmentExpression ||
        node.type === AST_NODE_TYPES.ClassDeclaration ||
        node.type === AST_NODE_TYPES.FunctionDeclaration ||
        node.type === AST_NODE_TYPES.ImportDeclaration ||
        node.type === AST_NODE_TYPES.TSDeclareFunction ||
        node.type === AST_NODE_TYPES.TSEnumDeclaration ||
        node.type === AST_NODE_TYPES.TSInterfaceDeclaration ||
        node.type === AST_NODE_TYPES.TSModuleDeclaration ||
        node.type === AST_NODE_TYPES.TSTypeAliasDeclaration ||
        node.type === AST_NODE_TYPES.VariableDeclaration
}
