// Type definitions for estraverse 5.1
// Project: https://github.com/estools/estraverse
// Definitions by: Sanex3339 <https://github.com/sanex3339>
//                 Jason Kwok <https://github.com/JasonHK>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped


// Override due to correspondance with TSESTree
// Link between Node from @types/estree and ast from typescript-estree types ?
// That's ugly but guess is due to a wrong original typing
// With visitorKeys, estraverse is supposed to support other Node types but there is no generics types


declare module 'estraverse' {
    import type { TSESTree } from '@typescript-eslint/typescript-estree/dist/ts-estree';
    import type { VisitorKeys } from "@typescript-eslint/visitor-keys"


    const Syntax: Syntax;

    const VisitorKeys: VisitorKeys;

    enum VisitorOption {
        Break,
        Skip,
        Remove,
    }

    class Controller {
        /**
         * Traverse the AST.
         */
        traverse(root: TSESTree.Node, visitor: Visitor): void;

        /**
         * Traverse and replace the AST.
         */
        replace(root: TSESTree.Node, visitor: Visitor): TSESTree.Node;

        /**
         * The current node.
         */
        current(): TSESTree.Node;

        /**
         * The type of current node.
         */
        type(): string;

        /**
         * Obtain the property paths array from root to the current node.
         */
        path(): Array<string | number> | null;

        /**
         * An array of parent elements.
         */
        parents(): TSESTree.Node[];

        /**
         * Notify the controller to break the traversals, skip the child nodes of current node or remove the
         * current node.
         */
        notify(flag: VisitorOption): void;

        /**
         * Break the traversals.
         */
        break(): void;

        /**
         * Skip the child nodes of current node.
         */
        skip(): void;

        /**
         * Remove the current node.
         */
        remove(): void;
    }

    function traverse(root: TSESTree.Node, visitor: Visitor): void;

    function replace(root: TSESTree.Node, visitor: Visitor): TSESTree.Node;

    function attachComments(tree: TSESTree.Node, providedComments: TSESTree.Comment[], tokens: TSESTree.Node[]): TSESTree.Node;

    // function cloneEnvironment(): typeof ESTraverse; // module estraverse was a namespace but not sure to need cloneEnvironment

    type NodeType =
        | "AssignmentExpression"
        | "AssignmentPattern"
        | "ArrayExpression"
        | "ArrayPattern"
        | "ArrowFunctionExpression"
        | "AwaitExpression"
        | "BlockStatement"
        | "BinaryExpression"
        | "BreakStatement"
        | "CallExpression"
        | "CatchClause"
        | "ClassBody"
        | "ClassDeclaration"
        | "ClassExpression"
        | "ComprehensionBlock"
        | "ComprehensionExpression"
        | "ConditionalExpression"
        | "ContinueStatement"
        | "DebuggerStatement"
        | "DirectiveStatement"
        | "DoWhileStatement"
        | "EmptyStatement"
        | "ExportAllDeclaration"
        | "ExportDefaultDeclaration"
        | "ExportNamedDeclaration"
        | "ExportSpecifier"
        | "ExpressionStatement"
        | "ForStatement"
        | "ForInStatement"
        | "ForOfStatement"
        | "FunctionDeclaration"
        | "FunctionExpression"
        | "GeneratorExpression"
        | "Identifier"
        | "IfStatement"
        | "ImportExpression"
        | "ImportDeclaration"
        | "ImportDefaultSpecifier"
        | "ImportNamespaceSpecifier"
        | "ImportSpecifier"
        | "Literal"
        | "LabeledStatement"
        | "LogicalExpression"
        | "MemberExpression"
        | "MetaProperty"
        | "MethodDefinition"
        | "ModuleSpecifier"
        | "NewExpression"
        | "ObjectExpression"
        | "ObjectPattern"
        | "Program"
        | "Property"
        | "RestElement"
        | "ReturnStatement"
        | "SequenceExpression"
        | "SpreadElement"
        | "Super"
        | "SwitchStatement"
        | "SwitchCase"
        | "TaggedTemplateExpression"
        | "TemplateElement"
        | "TemplateLiteral"
        | "ThisExpression"
        | "ThrowStatement"
        | "TryStatement"
        | "UnaryExpression"
        | "UpdateExpression"
        | "VariableDeclaration"
        | "VariableDeclarator"
        | "WhileStatement"
        | "WithStatement"
        | "YieldExpression";

    interface Syntax extends Record<NodeType, NodeType> {}

    interface Visitor {
        enter?: (this: Controller, node: TSESTree.Node, parent: TSESTree.Node | null) => VisitorOption | TSESTree.Node | void;

        leave?: (this: Controller, node: TSESTree.Node, parent: TSESTree.Node | null) => VisitorOption | TSESTree.Node | void;

        fallback?: 'iteration' | ((this: Controller, node: TSESTree.Node) => string[]);

        keys?: VisitorKeys;
    }
}
