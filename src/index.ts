import { setAnalysis } from './engine/analysis/analysis'
import { esStringToAst, reduceNodes, findNode, TSESTree } from './lib/ast'
import { setMemory, Memory } from './lib/astMemory/memory'
import { setEnterTraverseScope, TraverseScope, Scope, setLeaveTraverseScope } from './lib/astMemory/scope'

function main(esString: string, filePath: string, lang: 'js' | 'ts') {
  try {
    const { program } = esStringToAst(esString, { filePath, lang })


    type Test = { memory: Memory } & TraverseScope

    const acc: Test = {
      memory: new WeakMap(),
      currentScope: program,
      parentScope: []
    }

    return program

    // const setMemoryWrapper = (acc: Test, node: TSESTree.Node) => {
    //   const { memory, ...scope } = acc
    //   return { memory: setMemory(acc.memory, node, scope), ...scope } as Test
    // }

    // const setEnterTraverseScopeWrapper = (acc: Test, node: TSESTree.Node) => {
    //   const { memory, ...scope } = acc
    //   return { memory, ...setEnterTraverseScope(scope, node) }
    // }

    // const setLeaveTraverseScopeWrapper = (acc: Test, node: TSESTree.Node) => {
    //   const { memory, ...scope } = acc
    //   return { memory, ...setLeaveTraverseScope(scope, node) }
    // }

    // const enter = setAnalysis(setEnterTraverseScopeWrapper, setMemoryWrapper)
    // const leave = setAnalysis(setLeaveTraverseScopeWrapper)


    // const t = reduceNodes(acc, program, enter, leave)

    // return t.memory.get(program)



  } catch(err) {
    // save the trace
    throw new Error(err.message)
  }
  
}

export default main