import typescript from 'rollup-plugin-typescript2';
import pkg from './package.json';
import { terser } from "rollup-plugin-terser";

const input = 'src/index.js'

const external = [ ...Object.keys(pkg.dependencies || {})]

const plugins = [
  typescript({
    typescript: require('typescript'),
  }),
  terser() // minifies generated bundles
]

const output = [
  {
    file: pkg.main,
    format: 'cjs'
  }
]

if (pkg.module) output.push({ file: pkg.module, format: 'es' })
if (pkg.browser) output.push({ format: 'iife', name: 'MyPackage' })

export default {
  input,
  output,
  external,
  plugins
};